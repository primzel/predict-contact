import itertools
import csv
import json
import argparse
import requests
import sqlite3

conn = sqlite3.connect('data.db')
c = conn.cursor()

parser = argparse.ArgumentParser()
parser.add_argument("-t","--task",help="python generate.py --task task-name",required=True)
args = parser.parse_args()

def migrate():
    q = """
      CREATE TABLE numbers
      (
        number TEXT PRIMARY KEY,
        code TEXT,
        isValid INTEGER
      );
    """
    c.execute(q)
    conn.commit()
def isValidNumber(number):


    finalURL="http://www.cellsaa.com/trace-mobile-number"
    payload = {'mobilenumber': number, 'submit': "Search","submitse":"Trace"}
    r = requests.post(finalURL, data=payload)
    #PRINT RESPONSE
    print(r.status_code, r.reason) #HTTP
    return r.text.__contains__("Mobile Number Details - Complete Report")



# --------------------------------------------------------------------------------------------
def validate(length=10000):
    q="SELECT number FROM numbers WHERE isValid ISNULL LIMIT ?;"
    c.execute(q,(length,))
    for row in c.fetchall():
        isValid=isValidNumber(row[0])
        if isValid:
            q="UPDATE numbers set isValid=? WHERE number=?"
            c.execute(q,(isValid,row[0]))
        else:
            q="DELETE FROM numbers WHERE number=?"
            c.execute(q,(row[0],))
        conn.commit()

def populate():
    numbers=["".join(seq) for seq in itertools.product("1234567890", repeat=5)]
    with open('code', 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            list=[]
            for number in numbers:
                try:
                    num="%s%s46" % (row[0],number)

                    list.append([num,row[0]])
                except Exception as ex:
                    print("%s%s46" % (row,number))

            print("%s,%s"%(row[0],len(list)))
            q="INSERT INTO numbers(number,code) VALUES(?,?)"
            c.executemany(q,list)
    conn.commit()

def searchOnFaceBook():
    from selenium import webdriver
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.common.exceptions import TimeoutException
    from selenium.webdriver.common.by import By

    vconn = sqlite3.connect('data.db')
    vc = vconn.cursor()

    driver = webdriver.Firefox()

    driver.get("https://www.facebook.com/login/identify")
    # assert "Python" in driver.title
    elem = driver.find_element_by_id("identify_email")
    elem.clear()
    elem.send_keys("03244609898")
    elem.send_keys(Keys.RETURN)
    delay = 10 # seconds
    try:
        xpath='//*[@id="initiate_interstitial"]/div[2]/table/tbody/tr/td[2]/div/div/div[2]/div'
        element_present = EC.presence_of_element_located((By.XPATH, xpath))
        WebDriverWait(driver, delay).until(element_present)
        ele=driver.find_element_by_xpath(xpath)
        print(ele.text)
    except TimeoutException:
        print "Loading took too much time!"
    # assert "No results found." not in driver.page_source
    # driver.close()

def truncate_database():
    q="DELETE FROM numbers"
    c.execute(q)
    conn.commit()

if args.task=="populate":
    populate()
elif args.task=="truncate":
    truncate_database()
elif args.task=="validate":
    validate()
elif args.task=="migrate":
    migrate()
elif args.task=="search-on-facebook":
    searchOnFaceBook()
