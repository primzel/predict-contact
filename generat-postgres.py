import itertools
import csv
import json
import argparse
import requests
import psycopg2
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

conn = psycopg2.connect("dbname='contacts' user='postgres' host='localhost' password='postgres'")
c = conn.cursor()

parser = argparse.ArgumentParser()
parser.add_argument("-t","--task",help="python generate.py --task task-name",required=True)
parser.add_argument("-d","--database",help="python generate.py --task task-name --database <path-to-database>",required=False)
args = parser.parse_args()

def migrate():
    q = """
      CREATE TABLE numbers
      (
        number VARCHAR(11) PRIMARY KEY,
        profile_name TEXT,
        code VARCHAR (4),
        isValid BOOLEAN
      )
    """
    c.execute(q)
    conn.commit()
def isValidNumber(number):


    finalURL="http://www.cellsaa.com/trace-mobile-number"
    payload = {'mobilenumber': number, 'submit': "Search","submitse":"Trace"}
    r = requests.post(finalURL, data=payload)
    #PRINT RESPONSE
    print(r.status_code, r.reason) #HTTP
    return r.text.__contains__("Mobile Number Details - Complete Report")



# --------------------------------------------------------------------------------------------
def validate(length=10000):
    q="SELECT number FROM numbers WHERE isValid IS FALSE LIMIT %s" % (length)
    c.execute(q,{"limit":length})
    for row in c.fetchall():
        isValid=isValidNumber(row[0])
        if isValid:
            q="UPDATE numbers set isValid=%(isValid)s WHERE number=%(number)s"
            c.execute(q,{"isValid":isValid,"number":row[0]})
        else:
            q="DELETE FROM numbers WHERE number=%(number)s"
            c.execute(q,({"number":row[0]},))
        conn.commit()

def populate():
    numbers=["".join(seq) for seq in itertools.product("1234567890", repeat=5)]
    with open('code', 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            list=[]
            for number in numbers:
                try:
                    num="%s%s46" % (row[0],number)

                    list.append({"number":num,"code":row[0]})
                except Exception as ex:
                    print("%s%s46" % (row,number))

            print("%s,%s"%(row[0],len(list)))
            q="INSERT INTO numbers(number,code) VALUES(%(number)s,%(code)s)"
            c.executemany(q,list)
    conn.commit()

def find(driver,row):
    driver.get("https://www.facebook.com/login/identify")
    elem = driver.find_element_by_id("identify_email")
    elem.clear()
    elem.send_keys(row[0])
    elem.send_keys(Keys.RETURN)


def foundTest(driver,delay,row,xpath):
    element_present = EC.presence_of_element_located((By.XPATH, xpath))
    WebDriverWait(driver, delay).until(element_present)
    ele=driver.find_element_by_xpath(xpath)
    q="UPDATE numbers SET profile_name=%(pname)s where number=%(number)s"
    c.execute(q,{"number":row[0],"pname":ele.text})
    conn.commit()
    print(ele.text,row[0])


def searchOnFaceBook(limit=10000):

    # assert "Python" in driver.title
    delay = 5 # seconds
    q="SELECT number FROM numbers WHERE profile_name IS NULL limit %s"%(limit)
    c.execute(q)
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--incognito")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    for row in c.fetchall():
        flag=False
        try:
            find(driver,row)
        except Exception:
            exit()

        try:
            xpath='//*[@id="initiate_interstitial"]/div[2]/table/tbody/tr/td[2]/div/div/div[2]/div'
            foundTest(driver,delay,row,xpath=xpath)
            continue
        except Exception as ex:
            pass

        try:
            xpath='//*[@id="identify_yourself_flow"]/div/div[2]/div[1]/div[1]'
            foundTest(driver,0,row,xpath=xpath)
            continue
        except Exception as ex:
            pass

        try:
            xpath='//*[@id="u_0_4"]/div/div[2]/div[1]/table/tbody/tr/td/div/div/div/div[2]/div[1]'
            foundTest(driver,0,row,xpath=xpath)
            continue
        except Exception as ex:
            pass
        try:
            xpath='//*[@id="content"]/div/div[1]/div/div[2]/h2'
            foundTest(driver,0,row,xpath=xpath)
            continue
        except Exception as ex:
            print(row[0],"error")
            driver.close()
            driver = webdriver.Chrome(chrome_options=chrome_options)
            # q="UPDATE numbers SET profile_name=%(pname)s where number=%(number)s"
            # c.execute(q,{"number":row[0],"pname":driver.page_source})
            # conn.commit()

    # assert "No results found." not in driver.page_source


def truncate_database():
    q="DELETE FROM numbers"
    c.execute(q)
    conn.commit()

def migrate_data(database):
    import sqlite3
    liteConn=sqlite3.connect(database)
    clite=liteConn.cursor()
    clite.execute("SELECT * FROM numbers")
    list=[]
    for row in clite.fetchall():
        list.append({"number":row[0],"code":row[1],"isValid":row[2]==1})

    q="INSERT INTO numbers(number,code,isValid) VALUES(%(number)s,%(code)s,%(isValid)s)"
    c.executemany(q,list)
    conn.commit()

if args.task=="populate":
    populate()
elif args.task=="truncate":
    truncate_database()
elif args.task=="validate":
    validate()
elif args.task=="migrate":
    migrate()
elif args.task=="search-on-facebook":
    searchOnFaceBook()
elif args.task=="migrate-data":
    migrate_data(args.database)
